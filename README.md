# global-user
Reads and stores user data for a global user, like cross discord server items and buffs.

Port range 8200-8299

Requires [https://gitlab.com/discord-rpg-bot/model](https://gitlab.com/discord-rpg-bot/model) and name server to be running on port 9000

Also needs a mongodb database "globalUserDB" running on port 27017

Runtime documentation [http://localhost:8200/swagger-ui/index.html](http://localhost:8200/swagger-ui/index.html#/)