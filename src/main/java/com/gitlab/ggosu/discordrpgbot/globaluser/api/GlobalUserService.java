package com.gitlab.ggosu.discordrpgbot.globaluser.api;

import com.github.ggosu.discordbot.model.user.global.GlobalUser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public interface GlobalUserService {
    //global user

    /**
     * Adds a global user to the database
     */
    @PostMapping("/user/global")
    ResponseEntity<Object> addGlobalUser(@RequestBody GlobalUser globalUser);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Gets all global users
     */
    @GetMapping("/user/global")
    ResponseEntity<List<GlobalUser>> getAllGlobalUsers();
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Gets a global user by id
     */
    @GetMapping("/user/global/{id}")
    ResponseEntity<Optional<GlobalUser>> getGlobalUserById(@PathVariable String id);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes a global user by id
     */
    @DeleteMapping("/user/global/{id}")
    ResponseEntity<Object> deleteGlobalUserById(@PathVariable String id);
    // ---------------------------------------------------------------------------------------------------------------------

    // items
    /**
     * Adds a premium item to a global user
     */
    @PostMapping("/user/global/{id}/item/{itemId}/{amount}")
    ResponseEntity<Object> addPremiumItemToGlobalUser(@PathVariable String id, @PathVariable Integer itemId, @PathVariable Integer amount);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Removes a premium item from a global user
     */
    @DeleteMapping("/user/global/{id}/item/{itemId}/{amount}")
    ResponseEntity<Object> removePremiumItemFromGlobalUser(@PathVariable String id, @PathVariable Integer itemId, @PathVariable Integer amount);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes a premium item from a global user
     */
    @DeleteMapping("/user/global/{id}/item/{itemId}")
    ResponseEntity<Object> deletePremiumItemFromGlobalUser(@PathVariable String id, @PathVariable Integer itemId);
    // ---------------------------------------------------------------------------------------------------------------------

    //buffs
    /**
     * Adds a premium buff to a global user
     */
    @PostMapping("/user/global/{id}/buff/{buffId}")
    ResponseEntity<Object> addPremiumBuffToGlobalUser(@PathVariable String id, @PathVariable Integer buffId);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes a premium buff from a global user
     */
    @DeleteMapping("/user/global/{id}/buff/{buffId}")
    ResponseEntity<Object> deletePremiumBuffFromGlobalUser(@PathVariable String id, @PathVariable Integer buffId);
    // ---------------------------------------------------------------------------------------------------------------------
}
