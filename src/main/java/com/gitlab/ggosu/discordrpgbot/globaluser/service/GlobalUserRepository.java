package com.gitlab.ggosu.discordrpgbot.globaluser.service;

import com.github.ggosu.discordbot.model.user.global.GlobalUser;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GlobalUserRepository extends MongoRepository<GlobalUser, String> {

}