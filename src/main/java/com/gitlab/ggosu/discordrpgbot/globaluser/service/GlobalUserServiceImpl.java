package com.gitlab.ggosu.discordrpgbot.globaluser.service;

import com.github.ggosu.discordbot.model.user.UserBuff;
import com.github.ggosu.discordbot.model.user.UserItem;
import com.github.ggosu.discordbot.model.user.global.GlobalUser;
import com.gitlab.ggosu.discordrpgbot.globaluser.api.GlobalUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GlobalUserServiceImpl implements GlobalUserService {
    private final GlobalUserRepository globalUserRepository;

    // global user ------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addGlobalUser(GlobalUser globalUser) {
        globalUserRepository.insert(globalUser);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<List<GlobalUser>> getAllGlobalUsers() {
        List<GlobalUser> allGlobalUsers = globalUserRepository.findAll();
        if(allGlobalUsers.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(allGlobalUsers);
    }

    @Override
    public ResponseEntity<Optional<GlobalUser>> getGlobalUserById(String id) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(globalUser);
    }

    @Override
    public ResponseEntity<Object> deleteGlobalUserById(String id) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        globalUserRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    // items --------------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addPremiumItemToGlobalUser(String id, Integer itemId, Integer amount) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        // UserItem premiumItem
        for(UserItem premiumItem : globalUser.get().getPremiumItems()){
            if(Objects.equals(premiumItem.getId(), itemId)){
                premiumItem.setAmount(premiumItem.getAmount() + amount);
                globalUserRepository.save(globalUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }
        UserItem newPremiumItem = new UserItem(itemId, amount);
        globalUser.get().getPremiumItems().add(newPremiumItem);
        globalUserRepository.save(globalUser.get());
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Override
    public ResponseEntity<Object> removePremiumItemFromGlobalUser(String id, Integer itemId, Integer amount) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserItem premiumItem : globalUser.get().getPremiumItems()){
            if(Objects.equals(premiumItem.getId(), itemId)){
                if(premiumItem.getAmount() - amount < 0){
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                }
                if(premiumItem.getAmount() - amount == 0){
                    globalUser.get().getPremiumItems().remove(premiumItem);
                    globalUserRepository.save(globalUser.get());
                    return ResponseEntity.status(HttpStatus.OK).build();
                }
                premiumItem.setAmount(premiumItem.getAmount() - amount);
                globalUserRepository.save(globalUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<Object> deletePremiumItemFromGlobalUser(String id, Integer itemId) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserItem premiumItem : globalUser.get().getPremiumItems()){
            if(Objects.equals(premiumItem.getId(), itemId)){
                globalUser.get().getPremiumItems().remove(premiumItem);
                globalUserRepository.save(globalUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    // buffs --------------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addPremiumBuffToGlobalUser(String id, Integer buffId) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserBuff premiumBuff : globalUser.get().getPremiumBuffs()){
            if(Objects.equals(premiumBuff.getId(), buffId)){
                premiumBuff.setTimestamp(new Date());
                globalUserRepository.save(globalUser.get());
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
        }

        globalUser.get().getPremiumBuffs().add(new UserBuff(buffId, new Date()));
        globalUserRepository.save(globalUser.get());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Object> deletePremiumBuffFromGlobalUser(String id, Integer buffId) {
        Optional<GlobalUser> globalUser = globalUserRepository.findById(id);
        if(globalUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserBuff premiumBuff : globalUser.get().getPremiumBuffs()){
            if(Objects.equals(premiumBuff.getId(), buffId)){
                globalUser.get().getPremiumBuffs().remove(premiumBuff);
                globalUserRepository.save(globalUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
