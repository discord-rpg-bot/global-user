package com.gitlab.ggosu.discordrpgbot.globaluser.service;

import com.github.ggosu.discordbot.model.user.global.GlobalUser;
import com.gitlab.ggosu.discordrpgbot.globaluser.api.GlobalUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class GlobalUserController implements GlobalUserService {

    private GlobalUserServiceImpl globalUserService;

    // global user ---------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addGlobalUser(GlobalUser globalUser) {
        return globalUserService.addGlobalUser(globalUser);
    }

    @Override
    public ResponseEntity<List<GlobalUser>> getAllGlobalUsers() {
        return globalUserService.getAllGlobalUsers();
    }

    @Override
    public ResponseEntity<Optional<GlobalUser>> getGlobalUserById(String id) {
        return globalUserService.getGlobalUserById(id);
    }

    @Override
    public ResponseEntity<Object> deleteGlobalUserById(String id) {
        return globalUserService.deleteGlobalUserById(id);
    }

    // items ---------------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addPremiumItemToGlobalUser(String id, Integer itemId, Integer amount) {
        return globalUserService.addPremiumItemToGlobalUser(id, itemId, amount);
    }

    @Override
    public ResponseEntity<Object> removePremiumItemFromGlobalUser(String id, Integer itemId, Integer amount) {
        return globalUserService.removePremiumItemFromGlobalUser(id, itemId, amount);
    }

    @Override
    public ResponseEntity<Object> deletePremiumItemFromGlobalUser(String id, Integer itemId) {
        return globalUserService.deletePremiumItemFromGlobalUser(id, itemId);
    }

    // buffs ---------------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addPremiumBuffToGlobalUser(String id, Integer buffId) {
        return globalUserService.addPremiumBuffToGlobalUser(id, buffId);
    }

    @Override
    public ResponseEntity<Object> deletePremiumBuffFromGlobalUser(String id, Integer buffId) {
        return globalUserService.deletePremiumBuffFromGlobalUser(id, buffId);
    }
}
